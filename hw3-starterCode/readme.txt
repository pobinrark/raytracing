Assignment #3: Ray tracing

FULL NAME: Robin Park


MANDATORY FEATURES
------------------

<Under "Status" please indicate whether it has been implemented and is
functioning correctly.  If not, please explain the current status.>

Feature:                                 Status: finish? (yes/no)
-------------------------------------    -------------------------
1) Ray tracing triangles                  yes, 

2) Ray tracing sphere                     yes, explain!!!

3) Triangle Phong Shading                 yes, explain!!!

4) Sphere Phong Shading                   yes, explain!!!

5) Shadows rays                           yes, explain!!!

6) Still images                           yes, explain!!!
   
7) Extra Credit (up to 20 points)
   !!! explain your extra credit here, if applicable !!!
